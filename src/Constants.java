public interface Constants {
     String IN_FILE_TXT = "src\\files\\inFile.txt";
     String PATH_TO_MUSIC = "src\\music\\music.mp3";
     String INVALID_LINK = "Неверная ссылка на сайт";
     String INVALID_PATH = "Неверно указан путь к месту сохранения";
     String REG_EXP_FOR_PICTURES = "\\s*(?<=data-download\\s?=\\s?\")[^>].+?(?=\")";
     String REG_EXP_FOR_MUSIC = "\\s*(?<=data-url\\s?=\\s?\")[^>].+?(?=\")";
}
