import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Класс для воспроизведения
 * скаченной музыки
 *
 * @author Градсков Алексей, Кручинина Кристина,
 * Лисова Анастасия, 17ИТ17
 */
public class MP3Player extends Thread implements Constants {
    public void run() {
        try (FileInputStream inputStream = new FileInputStream(PATH_TO_MUSIC)) {
            try {
                Player player = new Player(inputStream);
                player.play();
            } catch (JavaLayerException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
