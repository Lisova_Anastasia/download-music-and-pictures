import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;

/**
 * Класс запуска потока
 * для скачивания файлов
 *
 * @author Градсков Алексей, Кручинина Кристина,
 * Лисова Анастасия, 17ИТ17
 */
public class DownloadThread extends Thread implements Constants {
    private String strUrl;
    private String nameFile;

    DownloadThread(String strUrl, String nameFile) {
        this.strUrl = strUrl;
        this.nameFile = nameFile;
    }

    public void run() {
        System.out.println("Скачивание началось");
        try {
            new FileOutputStream(nameFile).getChannel().
                    transferFrom(Channels.newChannel(new URL(strUrl).openStream()), 0, Long.MAX_VALUE);
        } catch (FileNotFoundException e) {
            System.out.println(INVALID_PATH);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Скачивание завершено");
    }
}
