import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс для скачивания, записи
 * и воспроизведеня музыки и
 * картинок
 *
 * @author Градсков Алексей, Кручинина Кристина,
 * Лисова Анастасия, 17ИТ17
 */
public class DownloadPictureAndPlayMusic implements Constants {
    private static int count = 0;
    private static ArrayList<String> downloadLinks = new ArrayList<>();
    private static final Object sync = new Object();

    public static void main(String[] args) throws IOException, InterruptedException {
        List<String> urlAndPath = readFile();
        if (!checkFile(urlAndPath)) {
            System.out.println("Неверные входные данные!");
            return;
        }

        List<String> firstString = Arrays.asList(urlAndPath.get(0).split(" "));
        String UrlMusic = firstString.get(0);
        if (!checksLinkMusic(UrlMusic)) {
            System.out.println(INVALID_LINK);
            return;
        }
        lookingForLinks(new URL(UrlMusic), REG_EXP_FOR_MUSIC);
        download(firstString.get(1));
        play();

        List<String> secondString = Arrays.asList(urlAndPath.get(1).split(" "));
        String urlPicture = secondString.get(0);
        if (!checksLinkPicture(urlPicture)) {
            System.out.println(INVALID_LINK);
            return;
        }

        lookingForLinks(new URL(urlPicture), REG_EXP_FOR_PICTURES);
        download(secondString.get(1));
    }

    /**
     * Возращает массив строк, прочитанных
     * из тектового файла
     *
     * @return массив строк
     */
    private static List<String> readFile() throws IOException {
        return Files.readAllLines(Paths.get(IN_FILE_TXT));
    }

    /**
     * Возвращает true, если количество
     * строк в файле равно двум, иначе
     * false
     *
     * @param urlAndPath массив, содержащий
     * строки файла
     * @return true, если строки две
     */
    private static boolean checkFile(List<String> urlAndPath){
        return urlAndPath.size() == 2;
    }

    /**
     * Возвращает true, если введённая
     * пользователем ссылка соответствует
     * "https://freephotos.cc/ru", иначе false
     *
     * @param url введённая пользователем ссылка
     * @return true, если ссылка верна
     */
    private static boolean checksLinkPicture(String url) {
        return url.matches("https://freephotos.cc/ru");
    }

    /**
     * Возвращает true, если введённая
     * пользователем ссылка соответствует
     * "https://muzika.vip/", иначе false
     *
     * @param url введённая пользователем ссылка
     * @return true, если ссылка верна
     */
    private static boolean checksLinkMusic(String url) {
        return url.matches("https://muzika.vip/");
    }

    /**
     * Ищет ссылки для скачивания на
     * заданном сайте и записывает их
     * в массив
     *
     * @param url ссылка на сайт
     * @param regEx регулярное выражение для
     * ссылок на скачивание
     */
    private static void lookingForLinks(URL url, String regEx) {
        String result = "";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Matcher matcher = Pattern.compile(regEx).matcher(result);
        int i = 0;
        while (matcher.find() && i < 1) {
            downloadLinks.add(matcher.group());
            i++;
        }
    }

    /**
     * Метод для параллельного
     * скачивания картинок и музыки
     * с помощью ссылок из массива
     * и записи по указанному пути
     *
     * @param path путь для записи файлов
     */
    private static void download(String path) throws InterruptedException {
        synchronized (sync) {
            Thread thread = new DownloadThread(downloadLinks.get(count), path);
            thread.start();
            thread.join();
            count++;
            sync.notify();
        }
    }

    /**
     * Метод для воспроизведения
     * музыкального файла
     */
    private static void play() throws InterruptedException {
        synchronized (sync) {
            while (!new File(PATH_TO_MUSIC).exists()) {
                sync.wait();
            }
            new MP3Player().start();
        }
    }
}

